app.controller('place_option', function($scope, $http) {
    $scope.a_place = []
    $scope.b_place = []

    $scope.country_list = []
    $scope.place_list = []
    $scope.place_with_country_list = {}

    $scope.process_api = function(name, obj) {
        console.log(obj)
        for (var ele in obj['routes']) {
            console.log(obj['routes'][ele]['legs'][0]['start_address'])
            console.log(obj['routes'][ele]['legs'][0]['end_address'])
            console.log(obj['routes'][ele]['legs'][0]['distance']['text'])
            console.log(obj['routes'][ele]['legs'][0]['duration']['text'])
            update('https://leekwunfung817assignment2.herokuapp.com/bookmarks/' + name, {
                name: name,
                start_address: obj['routes'][ele]['legs'][0]['start_address'],
                end_address: obj['routes'][ele]['legs'][0]['end_address'],
                distance: obj['routes'][ele]['legs'][0]['distance']['text'],
                duration: obj['routes'][ele]['legs'][0]['duration']['text']
            }, function(result) {
                // self.list.push(JSON.stringify(result))
                location.reload();
                return;
            });
        }
    }

    $http({
        method: 'GET',
        url: 'https://leekwunfung817assignment2.herokuapp.com/city_list.txt'
    }).then(function success(response) {
        var rows = response.data.split("\n")
        for (var row in rows) {
            if (row == 0) {
                continue;
            }
            var cells = rows[row].split(',');
            // console.log(cells[1]);
            if ($scope.place_with_country_list[cells[4]] == undefined) {
                // console.log(cells[4] + ' < init')
                $scope.place_with_country_list[cells[4]] = [];
            }
            if (cells[1] in $scope.place_with_country_list[cells[4]]) {

            }
            else {

                if ($scope.country_list.indexOf(cells[4]) == -1) {
                    $scope.country_list.push(
                        cells[4]
                    );
                }
                // console.log(cells[4] + ' < ' + cells[1])
                if ($scope.place_with_country_list[cells[4]].indexOf(cells[1]) == -1) {
                    $scope.place_with_country_list[cells[4]].push(cells[1]);
                }
            }
        }
        // console.log($scope.place_with_country_list);
    }, function error(response) {});
    $scope.$watch('choose_country', function() {
        console.log('choosing ' + $scope.choose_country)
        $scope.place_list = $scope.place_with_country_list[$scope.choose_country];
        console.log($scope.place_list);
    }, true);

    $scope.submit = function() {
        console.log('login cookie ' + document.cookie);
        console.log('https://leekwunfung817assignment2.herokuapp.com/MapDirection/' + $scope.a_place + '_To_' + $scope.b_place + '_In_' + $scope.choose_country + '?origin=' + $scope.a_place + '&destination=' + $scope.b_place + '&region=' + $scope.choose_country + '&key=AIzaSyAWGNEujW4hN4W13sPFWXGwkdXsySKniz0')
        alert($scope.a_place + ' To ' + $scope.b_place + ' In ' + $scope.choose_country)

        $http({
            method: 'POST',
            url: 'https://leekwunfung817assignment2.herokuapp.com/MapDirection/' + $scope.a_place + '_To_' + $scope.b_place + '_In_' + $scope.choose_country + '_By_' + JSON.parse(document.cookie).name + '?origin=' + $scope.a_place + '&destination=' + $scope.b_place + '&region=' + $scope.choose_country + '&key=AIzaSyAWGNEujW4hN4W13sPFWXGwkdXsySKniz0'
        }).then(function success(response) {
            $scope.process_api($scope.a_place + '_To_' + $scope.b_place + '_In_' + $scope.choose_country + '_By_' + JSON.parse(document.cookie).name, response.data)
        });
    };
});
