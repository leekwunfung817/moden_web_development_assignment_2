function table_head(arr) {
    var str = "<tr>";
    for (ele in arr) {
        str += "<th>";
        str += ele;
        str += "</th>";
    }
    str += "</tr>";
    return str;
}

function table_record(arr) {
    var str = "<tr>";
    for (ele in arr) {
        str += "<td>";
        str += ele;
        str += "</td>";
    }
    str += "</tr>";
    return str;
}

function del(url) {
    request(url, 'DELETE', {}, function(result) {
        console.log('Delete ' + url + ' ' + result)
    })
}

function name_array_to_keyvalue_array(n_arr, index, table) {
    console.log(n_arr)
    var arr = {}
    for (n in n_arr) {
        console.log(n + ' get ' + table + '_' + index + '_' + n + ' = ' + $('#' + table + '_' + index + '_' + n).val())
        arr[n] = $('#' + table + '_' + index + '_' + n).val();
    }
    console.log(JSON.stringify(arr));
    return arr;
}

function request(url, method, para, callback) {
    console.log(method + ' ' + url + ' ' + JSON.stringify(para))
    $.ajax({
        url: url,
        type: method,
        data: para,
        success: function(result) {
            callback(result);

        }
    });
}

function update(url, arr, callback) {
    request(url, 'PUT', arr, function(result) {
        console.log('Update ' + url + ' ' + result)
        if (callback != undefined) {
            callback();
        }
    })
}

function r() {
    var table = 'user';
    update('https://leekwunfung817assignment2.herokuapp.com/' + table + '/' + $('#r_name').val(), {
        name: $('#r_name').val(),
        last_name: $('#r_last_name').val(),
        email: $('#r_email').val(),
        password: $('#r_password').val()
    }, function(result) {
        alert('Welcome ' + $('#r_name').val() + ' to become our user!')

        $('#r_name').val('')
        $('#r_last_name').val('')
        $('#r_email').val('')
        $('#r_password').val('')
    });
}




// alert(document.cookie);
function request(url, method, para, callback) {
    console.log(method + ' ' + url + ' ' + JSON.stringify(para))
    $.ajax({
        url: url,
        type: method,
        data: para,
        success: function(result) {
            callback(result);

        }
    });
}

function set_page(arr) {
    console.log('set page')
    if (arr.name == undefined) {
        location.pathname = '/login/'
    }
    $('#title').html('Welcome ' + arr.name + '!');
    $('#name').html('Name: ' + arr.name + ' ' + arr.last_name);
    $('#last_name').html('Email: ' + arr.email);

    console.log(arr);

    if (JSON.parse(document.cookie).name != arr.name) {
        document.cookie = JSON.stringify(arr);
        location.reload();
    }
    else {
        document.cookie = JSON.stringify(arr);
    }

}
if (document.cookie == undefined ||
    document.cookie == '' ||
    document.cookie == '[object Object]' ||
    document.cookie.name == undefined ||
    document.cookie == '{}') {
    console.log('login cookie ' + document.cookie)
    request('https://leekwunfung817assignment2.herokuapp.com/isLogin', 'GET', {}, function(result) {
        console.log('login result ' + result)
        console.log('login cookie ' + document.cookie);
        var arr = JSON.parse(result)
        var cookie_arr = JSON.parse(document.cookie)
        if (arr['name'] != undefined && arr['name'] != '[object Object]') {
            console.log('arr.name = ' + arr.name)
            set_page(arr);
        }
        else if (cookie_arr['name'] != undefined && cookie_arr['name'] != '[object Object]') {
            console.log('cookie_arr.name = ' + cookie_arr.name)
            set_page(cookie_arr);
        }
        // else {
        // 	document.cookie = result + '';
        // 	set_page(arr);
        // }

    })
}
else {
    console.log('login cookie ' + document.cookie)
    set_page(JSON.parse(document.cookie));
}

app = angular.module('serviceToy', [])
