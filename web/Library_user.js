app.controller('user', function($scope, $http) {
    $scope.table = 'user'
    var self = this;
    self.list = []
    $scope.keys = [];
    $scope.hide = function(id) {
        $('#' + id).hide();
    }
    // angular js update a user
    $scope.update = function(index, arr) {
        if (arr['$$hashKey'] != undefined) {
            delete arr['$$hashKey']
        }
        console.log(index);
        console.log(arr);
        arr = name_array_to_keyvalue_array(arr, index, $scope.table);
        update('https://leekwunfung817assignment2.herokuapp.com/' + $scope.table + '/' + arr['name'], arr);
    }
    // angular js delete a user
    $scope.del = function(index, arr) {
        del('https://leekwunfung817assignment2.herokuapp.com/' + $scope.table + '/' + arr['name']);
    }
    // angular js add a user
    $scope.add = function(arr) {
        console.log(arr)
        update('https://leekwunfung817assignment2.herokuapp.com/' + $scope.table + '/' + arr['name'], arr, function(result) {
            // self.list.push(JSON.stringify(result))
            location.reload();
        });
    }
    $scope.key_list_to_value_array = function(table, list) {
        console.log(list)
        var arr = {}
        for (var ele in list) {
            arr[list[ele]] = $('#' + table + '_' + list[ele] + '_add').val();
            console.log(list[ele] + '=' + arr[list[ele]])
        }
        return arr;
    }
    // angular js user data initialize
    $http({
        method: 'GET',
        url: 'https://leekwunfung817assignment2.herokuapp.com/' + $scope.table + '/all'
    }).then(function success(response) {
        var key = []
        for (ele in response.data) {
            var add_array = response.data[ele];
            if (Object.keys(add_array).length != key.length) {
                for (ele in add_array) {
                    key.push(ele);
                }
            }
            $scope.keys = key;
            self.list.push(add_array);
        }
    }, function error(response) {});
});
