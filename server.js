var firebase_app = require("./library_firebase")

function select(path, callback) {
  console.log('select ' + path);
  var ref = firebase_app.database().ref(path);
  ref.once('value').then(function(snap) {
    callback(snap.val())
  });
}

function update(path, arr, callback) {
  delete arr['$$hashKey'];
  console.log('update ')
  console.log(arr)

  // for ele in arr:
  firebase_app.database().ref(path).set(arr);
  select(path, function(arr) {
    callback(arr)
  });
}

function firebase_del(path) {
  console.log('DELETE ' + path)
  firebase_app.database().ref(path).remove();
}

html_helper = require("./library_html_helper")
a = html_helper.a
form = html_helper.form
get_form = html_helper.get_form

fs = require("fs")
request = require('request');
router = require('./library_router');

function request_JSON(url, callback) {
  request_func(url, function(json) {
    callback(JSON.parse(json));
  })
}

function request_func(url, callback) {
  request(url, function(error, response, body) {
    try {
      callback(body)
    }
    catch (e) {}
  });
}


function response_json(url, res) {
  request_func(
    url,
    function(json) {
      res.send(json)
    }
  );
}

function rj(u, r) {
  response_json(u, r)
}

function mrj(u) {
  return function(req, res) {
    rj(u, res);
  }
}
// progress main response json
function pmrj(mu, u) {
  router.get(mu, mrj(u));
}

function read_file_func(fn) {
  return fs.readFileSync(fn, 'utf8');
}
// response file url
function rfu(url, fn) {
  router.get(url, function(req, res) {
    res.send(read_file_func(fn));
  });
}

// response json array url
function rjru(url, arr) {
  router.get(url, function(req, res) {
    res.send(JSON.stringify(arr));
  });
}


// get request customed response
function grcr(url, callback) {
  console.log('GET route ' + url);
  router.get(url, function(req, res) {
    req.query['__proto__'] = undefined;
    var get_response = callback(req.params, req.query, res);
    if (get_response != undefined) {
      res.send(get_response);
    }
  });
}

// post request customed response
function porcr(url, callback) {
  console.log('POST route ' + url);
  router.post(url, function(req, res) {
    // console.log(req)
    req.query['__proto__'] = undefined;
    // res.send(callback(req.body, res));
    var get_response = callback(req.params, req.query, res);
    if (get_response != undefined) {
      res.send(get_response);
    }
  });
}

// delete request customed response
function dercr(url, callback) {
  console.log('DELETE route ' + url);
  router.del(url, function(req, res) {
    req.query['__proto__'] = undefined;
    var get_response = callback(req.params, req.query, res);
    if (get_response != undefined) {
      res.send(get_response);
    }
  });
}


// put request customed response
function purcr(url, callback) {
  console.log('PUT route ' + url);
  router.put(url, function(req, res) {
    // console.log(req)
    req.query['__proto__'] = undefined;
    // res.send(callback(req.body, res));
    var get_response = callback(req.params, req.body, res);
    if (get_response != undefined) {
      res.send(get_response);
    }
  });
}

logined_user = {}

// all the API URL and the response are build up here
function setRaspfulServer(name, dir, add_callback, del_callback, edit_callback) {
  var root_path = '/' + name;
  // grcr(root_path, get_callback);
  grcr(root_path, function(key, req, res) {
    console.log(key)
    console.log(req)
    var get_name = key.name + ''
    // delete req.name
    if (get_name != 'all') {
      get_name = '/' + get_name;
    }
    else {
      get_name = ''
    }
    select(dir + get_name, function(arr) {
      // if (req.name!=undefined&&req.password!=undefined&&arr.name!=undefined&&arr.password!=undefined) {
      //   if (req.name==arr.name&&req.password==arr.password) {

      //   }
      // }
      if (dir == '/project/user') {
        logined_user = arr;
      }
      console.log(arr)
      res.send(JSON.stringify(arr));
    });
  });
  porcr(root_path, add_callback);
  dercr(root_path, del_callback);
  purcr(root_path, edit_callback);
  /*
  get
  post
  delete
  put
  */
}
grcr('/isLogin', function(key, req, res) {
  res.send(JSON.stringify(logined_user));
  logined_user = {}
})
// rj(,logined_user);
const MapDirection_dir = '/project/data/MapDirection';
// This is the third party API of finding the Map direction
const MapDirection_url = 'https://maps.googleapis.com/maps/api/directions/json';
setRaspfulServer('MapDirection/:name', MapDirection_dir,
  // function(key, req, res) {
  //   var url = MapDirection_url + '?origin=' + req.origin + '&destination=' + req.destination + '&region=' + req.region + '&key=' + req.key;
  //   request_func(url, function(json) {
  //     res.send(json)
  //   });
  // },
  function(key, req, res) {
    var url = MapDirection_url + '?origin=' + req.origin + '&destination=' + req.destination + '&region=' + req.region + '&key=' + req.key;
    request_func(url, function(json) {
      update(MapDirection_dir + '/' + req.index,
        JSON.parse(json),
        function(arr) {
          res.send('' + JSON.stringify(arr));
        });
    });
  },
  function(key, req, res) {
    firebase_del(MapDirection_dir + '/' + req.index);
    res.send(JSON.stringify(req));
  },
  function(key, req, res) {
    // for (ele in req) {
    //   console.log(ele + ':' + req[ele])
    // }
    // update(MapDirection_dir + '/' + req.index,
    //   req,
    //   function(arr) {
    //     res.send(JSON.stringify(arr));
    //   });

    var add = (function() {
      var count = 0;
      return function() { return count += 1; }
    })
    for (var ele in req) {
      update(bookmarks_dir + '/' + name + '/' + ele, req[ele], function(arr) {
        var count = add();
        console.log((req.length - 1) + ' == ' + count);
        if ((req.length - 1) == count) {
          console.log('finish')
        }
      });
      // res.send(JSON.stringify(req));
    }
    return JSON.stringify(req);
  });

const User_dir = '/project/user';
setRaspfulServer('user/:name', User_dir,
  // function(key, req, res) {
  //   var name = key.name + ''
  //   delete req.name
  //   if (name != 'all') {
  //     name = '/' + name
  //   } else {
  //     name = ''
  //   }
  //   select(User_dir + name, function(arr) {
  //     console.log(arr)
  //     res.send(JSON.stringify(arr));
  //   });
  // },
  function(key, req, res) {
    var name = key.name
    delete req.name
    update(User_dir + '/' + name, req, function(arr) {
      res.send(JSON.stringify(arr));
    });
  },
  function(key, req, res) {
    firebase_del(User_dir + '/' + key.name);
    res.send(JSON.stringify(key));
  },
  function(key, req, res) {
    for (ele in req) {
      console.log(ele + ':' + req[ele])
    }
    var name = key.name
    // delete req.name
    update(User_dir + '/' + name, req, function(arr) {
      // res.send(JSON.stringify(req));
    });
    return JSON.stringify(req);

    var add = (function() {
      var count = 0;
      return function() { return count += 1; }
    })
    for (var ele in req) {
      update(bookmarks_dir + '/' + name + '/' + ele, req[ele], function(arr) {
        var count = add();
        console.log((req.length - 1) + ' == ' + count);
        if ((req.length - 1) == count) {
          console.log('finish')
        }
      });
    }
    return JSON.stringify(req);
  });

const bookmarks_dir = '/project/bookmarks';
setRaspfulServer('bookmarks/:name', bookmarks_dir,
  // function(key, req, res) {
  //   var name = key.name + ''
  //   delete req.name
  //   select(bookmarks_dir + '/' + name, function(arr) {
  //     res.send(JSON.stringify(req));
  //   });
  // },
  function(key, req, res) {
    var name = key.name + ''
    delete req.name
    update(bookmarks_dir + '/' + name, req, function(arr) {
      res.send(JSON.stringify(req));
    });
  },
  function(key, req, res) {
    var name = key.name
    firebase_del(bookmarks_dir + '/' + name);
    res.send(JSON.stringify(req));
  },
  function(key, req, res) {
    var name = key.name
    // delete req.name
    // Closure
    var add = (function() {
      var count = 0;
      return function() { return count += 1; }
    });
    for (var ele in req) {
      update(bookmarks_dir + '/' + name + '/' + ele, req[ele], function(arr) {
        // var count = add();
        // console.log((req.query.length - 1) + ' == ' + count);
        // if ((req.query.length - 1) == count) {
        //   console.log('finish')
        // }
      });
    }
    res.send(JSON.stringify(req));
    // return JSON.stringify(req.query);
  });
