
function a(text, url) {
  if (url == undefined) {
    url = text;
  }
  var result = '<a href="' + url + '">' + text + '</a><br>';
  // console.log(result);
  return result;
}

function form(root_dir, list) {
  var str = '';
  str += root_dir;
  str += '<br>';
  str += '<form action="' + root_dir + '" method="post">';
  str += '<table>';
  for (item in list) {
    str += '<tr><td>' + item + ' : </td><td><input type="text" name="' + item + '" value="' + list[item] + '"/></td></tr>';
    str += '<br>';
  }
  str += '<tr><td><input type="submit"/></td></tr>';
  str += '</table>';
  str += '<br>';
  str += '</form><hr>';
  // console.log(str)
  return str;
}

function get_form(root_dir, list) {
  var str = '';
  str += root_dir;
  str += '<br>';
  str += '<form action="' + root_dir + '" method="get">';
  str += '<table>';
  for (item in list) {
    str += '<tr><td>' + item + ' : </td><td><input type="text" name="' + item + '" value="' + list[item] + '"/></td></tr>';
    // str += '<br>';
  }

  str += '<tr><td><input type="submit"/></td></tr>';
  str += '</table>';
  str += '</form><hr>';
  return str;
}
module.exports = {
    a:a,
    form:form,
    get_form,get_form
}